<head>
<style>
#question{
	color: #22ff8a;
	font-size: 50px;
}
#answer{
	color: blue;
	font-size: 56px;
}
#variants {
	display: inline-flex;
	font-size: 21px;
}
.variants {
	padding: 0 10px;
}
#variant_1 {
	color: #ff0001;
}
#variant_2 {
	color: #00ff02;
}
#variant_3 {
	color: #000104;
	font-family: cursive, "Times New Roman";
}
#variant_4 {
	color: #000002;
	font-family: sans-serif;
}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0, minimal-ui"/>
</head>
<body>
<?php 
    $countAllQuestions = count($allQuestions); 
    $randQuestionInd = rand(0, $countAllQuestions-1);
	$question = $allQuestions[$randQuestionInd]->question; 
	$variant_1 = $allQuestions[$randQuestionInd]->variant_1; 
	$variant_2 = $allQuestions[$randQuestionInd]->variant_2; 
	$variant_3 = $allQuestions[$randQuestionInd]->variant_3; 
	$variant_4 = $allQuestions[$randQuestionInd]->variant_4; 
?>
<p id="question">{{  $question }}</p>
<div id="variants">
	<p id="variant_1" class="variants">{{  $variant_1 }}</p>
	<p id="variant_2" class="variants">{{  $variant_2 }}</p>
	<p id="variant_3" class="variants">{{  $variant_3 }}</p>
	<p id="variant_4" class="variants">{{  $variant_4 }}</p>
</div>
    <?php $answer = $allQuestions[$randQuestionInd]->answer; ?>
<p id="answer" style="display: none"> {{ $answer }} </p>
<script>
$(document).ready(function(){
	
	var random_red_color = Math.floor(Math.random() * 255);
	var random_green_color = Math.floor(Math.random() * 255);
	var random_blue_color = Math.floor(Math.random() * 255);
	$('#answer').css('color', 'rgb(' + random_red_color + ',' + random_green_color + ',' + random_blue_color + ')');
	$('#question').click(function(){
		$('#answer').show();
		//setTimeout(function(){location.reload();}, 3000);
	});
});
</script>
</body>
