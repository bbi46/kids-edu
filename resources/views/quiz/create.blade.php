<?php
use App\Http\Controllers\QuizController;
?>
@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif
<?php
echo Form::open(['action' => 'QuizController@store', 'method' => 'post']);
echo Form::label('question', 'Question');
echo Form::text('question');
echo Form::label('variant_1', 'Variant 1');
echo Form::text('variant_1');
echo Form::label('variant_2', 'Variant 2');
echo Form::text('variant_2');
echo Form::label('variant_3', 'Variant 3');
echo Form::text('variant_3');
echo Form::label('variant_4', 'Variant 4');
echo Form::text('variant_4');
echo Form::label('answer', 'Answer');
echo Form::text('answer');
echo Form::submit('Add!');
echo Form::close();