<!DOCTYPE html> 

<head>
    <meta charset="utf-8">
    <title>Змейка</title>
    <link rel="stylesheet" id="style" type="text/css" href="{{ URL::asset('css/games/snake/main-snake.css') }}" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>
    <div id="mode-wrapper">Выберите режим<br />
	<button id="Easy">Лёгкий</button><br />
    <button id="Medium">Средний</button><br />
	<button id="Difficult">Тяжёлый</button></div>
    <button id="high-score">Смотреть максимальное количество очков</button>
    <div id="game-area" tabindex="0">
    </div>
    <script type="text/javascript" src="{{ URL::asset('/js/games/snake/snake.js') }}"></script>
    <script type="text/javascript">
    var mySnakeBoard = new SNAKE.Board({
        boardContainer: "game-area",
        fullScreen: true
    });    
    </script>
</body>
</html>
