<html>
   <head>
      <title>Ajax Example</title>
     <meta name="csrf" content=" {{ csrf_token() }}">
      <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js">
      </script>
      
      <script>
         function getMessage() {
			 $.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});
            $.ajax({
               type:'POST',
			   dataType : 'JSON',
               url:'/getmsg',
			   headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			   },
               data:'_token = <?php echo csrf_token() ?>',
               success:function(data) {
                  $("#msg").html(data.msg);
               },
			    error: function( json )
            {
				alert(json.status);
                if(json.status === 422) {
                    var errors = json.responseJSON;
                    $.each(json.responseJSON, function (key, value) {
                        $('.'+key+'-error').html(value);
                    });
                } else {
                    // Error
                    // Incorrect credentials
                    // alert('Incorrect credentials. Please try again.')
                }
            }
            });
         }
      </script>
   </head>
   
   <body>
      <div id = 'msg'>This message will be replaced using Ajax. 
         Click the button to replace the message.</div>
      <?php
	    use Illuminate\Support\Facades\URL;
	     echo Form::open(['action'=>'http://kids-edu.tmweb.ru/getmsg']);
         echo Form::submit('Replace Message',['onClick'=>'getMessage()']);
		 echo Form::close();
      ?>
   </body>

</html>
