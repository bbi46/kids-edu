<head>
<style>
#eat-ueat-word{
	 font-size: 50px;
}
#eat-ueat-translate{
	 font-size: 55px;
}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0, minimal-ui"/>
</head>
<body>
<p id="eat-ueat-word">{{ $words[$randId]->word }}</p>
<p id="eat-ueat-translate" style="display: none;">{{ $words[$randId]->translate_ru }}</p>
@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif
<?php
use App\Http\Controllers\EatableUneatableController;
echo Form::open(['action' => 'EatableUneatableController@store', 'method' => 'post']);
echo Form::label('word', 'Word');
echo Form::text('word');
echo Form::label('transcription', 'Transcription');
echo Form::text('transcription');
echo Form::label('translate_ru', 'Russian Translation');
echo Form::text('translate_ru');
echo Form::submit('Add!');
echo Form::close();
?>
<p style="font-size: 46px; text-align: center; color: blue;">
Всього слів:
<span style="color: green; font-size: 52px; font-weight: bold;">
{{ count($words) }}	
</span>
</p>
<script>
$(document).ready(function(){
	var random_red_color = Math.floor(Math.random() * 255);
	var random_green_color = Math.floor(Math.random() * 255);
	var random_blue_color = Math.floor(Math.random() * 255);
	$('#eat-ueat-translate').css('color', 'rgb(' + random_red_color + ',' + random_green_color + ',' + random_blue_color + ')');
	$('#eat-ueat-word').click(function(){
		$('#eat-ueat-translate').show();
	});
});
</script>
</body>