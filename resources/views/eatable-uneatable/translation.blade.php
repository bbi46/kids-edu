<head>
<script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script>
$(document).ready(function(){
	 function getTranslation() {
		$.ajax({
		   type:'POST',
		   url:'/gettranslation',
		   data:'_token = <?php echo csrf_token() ?>, word = <?php echo $words[$randId]->word ?> ',
		   success:function(data) {
			  $("#translate").html(data.msg);
		   }
		});
	 }
});
</script>
</head>
<body>
<div id = 'translate'>
	<?php echo Form::button('Translate',['onClick'=>'getTranslation()']); ?>
</div>
</body>