<?php

use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/guess-thing', function () {
	$things = DB::select('select * from guess_thing where id != ?', [0]);
	$thingInd = array_rand($things, 1);
	return view('guess-thing/index', ['things' => $things, 'i' => $thingInd]);
});

Route::get('/quiz', function(){
   $quizArr = [
   		" @@ волка @@ зайца @@ --лисы (объяснение: Патрикеевна) @@ курочки Рябы",
   		"11.Кто тянул репку предпоследним? @@ --кошка @@ собака @@ мышка",
   		"12.Что пекла Маша у медведя в лесу? @@ пиццу @@ торт @@ --пирожки @@ каравай",
   		"13.Сколько разбойников повстречал Али-баба? @@ 10 @@ 20 @@ 30 @@ --40",
   		"14.Почему облака могут плыть по небу, не падая на землю? @@ потому что в небе, как и в космосе, работают законы гравитации @@ потому что они невесомы @@ --потому что их поддерживает потом воздуха, направленный от земли",
   ];

   $question = $quizArr[array_rand($quizArr, 1)];
   $questionArr = explode('@@', $question);
   $questions = DB::select('select * from quiz where id != ?', [0]);
   return view('quiz/index', 
      ['quiz' => $quizArr, 'question' => $questionArr, 'allQuestions' => $questions]);
});

Route::get('/quiz/create', function(){
	return view('quiz/create');
});
Route::post('/quiz/create', 'QuizController@store');

Route::get('/cards', function(){
	$cards = DB::select('select * from cards_stories where id != ?', [0]);
	return view('cards/stories', ['stories' => $cards]);
});

Route::get('/play/snake', function(){
   return view('games/JavaScript-Snake/index');
});

Route::get('/speed-telling', function(Request $request){
	$category = $request->get('cat');
	$number = $request->get('number');
    $categories = DB::select('select * from words_cats where id != ?', [0]);
	$words = DB::select('select * from words where cat_id = ?', [$category]);
	$words_s = DB::select('select * from words_slovos where id != ?', [0]);
	return view('/speed-telling', [
      'cats' => $categories, 
      'words' => $words,
      'words_s' => $words_s,	  
      'number' => $number
      ]);
});

Route::post('/cards/create', 'CardsStories@store');
Route::get('/eatable-uneatable', function(){
   $words = DB::select('select * from eatable_uneatable where id != ?', [0]);
   $randWordId = array_rand($words, 1);

   return view('eatable-uneatable/index', 
      ['words' => $words, 'randId' => $randWordId]);
});
Route::post('/eatable-uneatable/create', 'EatableUneatableController@store');
Route::get('ajax',function() {
   return view('message');
});
Route::post('/getmsg','AjaxController@index');
