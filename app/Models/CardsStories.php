<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CardsStories extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql';
    protected $table = 'cards_stories';
	public $timestamps = false;
}