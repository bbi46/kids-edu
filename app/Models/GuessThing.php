<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GuessThing extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    public $timestamps = false;
	protected $connection = 'mysql';
	protected $table = 'guess_thing';
}