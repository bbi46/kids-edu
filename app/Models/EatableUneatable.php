<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EatableUneatable extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql';
    protected $table = 'eatable_uneatable';
	public $timestamps = false;
}