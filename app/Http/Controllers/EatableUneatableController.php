<?php
namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request as Request;
use App\Models\EatableUneatable;

class EatableUneatableController extends BaseController 
{
	public function store(Request $request){
		
		$cards = new EatableUneatable;
		$cards->word = $request->word;
		$cards->transcription = $request->transcription;
		$cards->translate_ru = $request->translate_ru;
		$cards->created = time();

		$validatedData = $request->validate([
        	'word' => 'required|unique:eatable_uneatable|max:255',
    	]);
		if($cards->save()){
			return redirect('/eatable-uneatable/')->with('status', 'saved!');
		}
	}
	public function translation(Request $request) {
      $msg = $request->word;
      return response()->json(array('msg'=> $msg), 200);
   }
}