<?php
namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request as Request;
use App\Models\Quiz;

class QuizController extends BaseController 
{
	public function store(Request $request){
		
		$question = new Quiz;
		$question->question = $request->question;
		$question->answer = $request->answer;
		$question->variant_1 = $request->variant_1 ? $request->variant_1 : 'not set';
		$question->variant_2 = $request->variant_2 ? $request->variant_2 : 'not set';
		$question->variant_3 = $request->variant_3 ? $request->variant_3 : 'not set';
		$question->variant_4 = $request->variant_4 ? $request->variant_4 : 'not set';
		$question->tag = $request->tag ? $request->tag : 'not set';
		$question->source = $request->source ? $request->source : '?';
		$question->created = time();
		if($question->save()){
			return redirect('/quiz/create')->with('status', 'saved!');
		}
	}
	public function translation(Request $request) {
      $msg = $request->answer;
      return response()->json(array('msg'=> $msg), 200);
   }
}