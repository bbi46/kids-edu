<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AjaxController extends Controller {
   public function index(Request $request) {
	   $response = array(
          'status' => 'success',
          'msg' => $request->message,
      );
      $msg = "This is a simple message.";
      return response()->json($response);
   }
}